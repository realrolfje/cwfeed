Listen to the headlines in CW
=============================

A little script to render headlines into morse code mp3 files. Nothing much, just
to do some training.

I'd like to create separate "render" script and make it smarter, only fetching 
new text, keeping the old ones. The "play" script then just plays the mp3 files
in an endless loop.

For now, it's just all-in-one and runs on OSX.
