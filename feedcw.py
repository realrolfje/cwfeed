#! /usr/bin/python
#
# Takes a bunch of rss feeds, renders them into morse code using lcwo.net
#
# To install feedparser library:
# sudo easy_install feedparser 
#
# Train.
#

import sys
import feedparser
import socket
import urllib
import random
import subprocess
 
cwcharspeed = str(20)
cweffspeed = str(20)
cwtone = str(600)
kochchars = " abcdefghijklmnopqrstuvwxyz0123456789.,/?="

timeout = 120
socket.setdefaulttimeout(timeout)

# all the feeds
rssfeeds = ["http://www.arrl.org/news/rss", \
			"https://www.sciencenews.org/feeds/headlines.rss", \
			"http://rss.slashdot.org/Slashdot/slashdot", \
			"http://www.kurzweilai.net/blog/feed/atom"]

# Load all feeds
rssentries = []
for feed in rssfeeds:
	print "Loading: " + feed
	rssentries = rssentries + feedparser.parse(feed).entries

# Shuffle all the entries to make it more interesting
random.shuffle(rssentries)

# Render all titles as CW mp3 files
i = 0
mp3files = []
for entry in rssentries:
	title = "= " + unicode(entry.title).encode("utf-8").lower()
	remove = title.translate(None, kochchars)
	title = title.translate(None, remove)
	print "Rendering: " + title

	queryparams = urllib.urlencode({'t' : title }).replace("+", "%20")
	lcwourl = "http://cgi3.lcwo.net/cgi-bin/cw.mp3?s=" \
				+ cwcharspeed + "&e=" + cweffspeed + "&f=" + cwtone \
				+ "&" + queryparams

	mp3file = "cw-" + "%03d" % (i,) + ".mp3"
	urllib.urlretrieve (lcwourl, mp3file)
	mp3files = mp3files + [mp3file]
	i = i + 1

# Play all the files (no network needed anymore)
for file in mp3files:
	print file
	subprocess.check_output(['afplay',file])
